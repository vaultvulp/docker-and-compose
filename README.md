docker-and-compose(1)
=====================

builds off of `docker` and adds `docker-compose`

## usage

```yaml
# .gitlab-ci.yml
image: registry.gitlab.com/citygro/docker-and-compose
# ...
```
